#!/bin/bash

# opcodes starting at 0x1380 -
# sancus-core/core/sim/rtl_sim/src/sancus/sancus_macros.asm

# opcodes:
# 0x1380 := sancus_disable
# 0x1381 := sancus_enable
# 0x1384 := sancus_wrap
# 0x1385 := sancus_unwrap
# 0x1386 := sancus_get_id
# 0x1387 := sancus_get_caller_id
# 0x1388 := sancus_stack_guard


# Initialize our own variables:
output_file=""
elf=""

declare -A opcodes
opcodes+=( ["0x1380"]=sancus_disable ["0x1381"]=sancus_enable ["0x1384"]=sancus_wrap ["0x1385"]=sancus_unwrap ["0x1386"]=sancus_get_id ["0x1387"]=sancus_get_caller_id ["0x1388"]=sancus_stack_guard )


function show_help() {
    echo "Usage: $0 [-h] [-f] [-o OUTPUT] -i ELF"
    echo "-h            -- show this page"
    echo "-f            -- find the new opcodes in ELF"
    echo "-o OUTPUT     -- write objdump to output file and replace the new opcodes"
    echo "-i ELF        -- binary file to inspect"
}


# verify if there's an inputfile
function verify_input() {
    if [ -z $elf ]; then
        echo "Error: missing inputfile"
        echo ""
        show_help
        exit 1
    fi
}


# find and print out the new cpu opcodes
function find_opcodes() {
    verify_input
    msp430-objdump -fhD $elf | grep '8[0-9,a-f] 13' | grep '0x138[0-9,a-f]' > $tmpfile
    replace_opcodes
    cat $tmpfile
}


# search and replace all keys from opcode-dictionary
replace_opcodes() {
    for key in ${!opcodes[@]}; do
        sed -i "s/${key}/${opcodes[${key}]}/g" $tmpfile
    done
}


# generate objdump and replace the opcodes with function names
# from opcode-dictionary
function gen_objdump() {
    verify_input
    msp430-objdump -fhD $elf > $tmpfile
    replace_opcodes
    if [ -z $output_file ]; then
        less $tmpfile
    else
        cat $tmpfile | tee $output_file
    fi
}


# main function
function main() {
    tmpfile=$(mktemp)
    if [ $find ]; then
        find_opcodes $elf
    else
        gen_objdump $elf
    fi
    rm -f $tmpfile
}


# get command line arguments
while getopts "hfo:i:" opt; do
    case "$opt" in
        h)
            show_help
            exit 0
            ;;
        o)
            output_file=$OPTARG
            ;;
        i)
            elf=$OPTARG
            ;;
        f)
            find=true
            ;;
    esac
done

## main() starts here
main
exit 0
