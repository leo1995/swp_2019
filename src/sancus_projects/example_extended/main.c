#include <msp430.h>
#include <stdio.h>
#include <sancus/sm_support.h>
#include <sancus_support/sm_io.h>
#include "reader_sm1.h"
#include "reader_sm2.h"
#include "reader_malicious.h"


int main()
{
    msp430_io_init();

    pr_info("enabling SENSOR with VENDOR ID: 0x1234");
    sancus_enable(&sensor);
    pr_sm_info(&sensor);

    pr_info("enabling READER 1 with VENDOR ID: 0x6789");
    sancus_enable(&reader_sm1);
    pr_sm_info(&reader_sm1);

    pr_info("enabling READER 2 with VENDOR ID: 0x1312..");
    sancus_enable(&reader_sm2);
    pr_sm_info(&reader_sm2);

    pr_info("requesting sensor readings for READER 1..");
    nonce_t no = 0xabcd;
    ReaderOutput out1;
    get_readings_sm1(no, &out1);

    pr_info("requesting sensor readings for READER 2..");
    ReaderOutput out2;
    get_readings_sm2(no, &out2);

    pr_info("dumping sealed output from READER 1");
    dump_buf((uint8_t*)&no, sizeof(no), "  Nonce");
    dump_buf((uint8_t*)&out1.cipher, sizeof(out1.cipher), "  Cipher");
    dump_buf((uint8_t*)&out1.tag, sizeof(out1.tag), "  Tag");

    pr_info("dumping sealed output from READER 2..");
    dump_buf((uint8_t*)&no, sizeof(no), "  Nonce");
    dump_buf((uint8_t*)&out2.cipher, sizeof(out2.cipher), "  Cipher");
    dump_buf((uint8_t*)&out2.tag, sizeof(out2.tag), "  Tag");

    pr_info("all done!");

    pr_info("Now checkout with a malicious software module");
    pr_info("enabling MALICIOUS READER with VENDOR ID: 0x9876");
    sancus_enable(&reader_malicious);
    pr_sm_info(&reader_malicious);

    pr_info("try to steal data from READER 1..");
    tamper_readings_sm1();

    EXIT();
}
