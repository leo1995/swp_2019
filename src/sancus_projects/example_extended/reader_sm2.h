#ifndef READER_SM2_H
#define READER_SM2_H

#include "reader.h"

extern struct SancusModule reader_sm2;

void SM_ENTRY(reader_sm2) get_readings_sm2(nonce_t no, ReaderOutput* out);

#endif
