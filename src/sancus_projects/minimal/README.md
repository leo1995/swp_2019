# Minimal Example for using the Sancus API

## Description

This is a minimal example for writing C-source code with a Sancus-enabled software module.

A software module with the name _my\_sm_ gets initialized and enabled with an entry point _hello\_entry()_. The module just prints out its _ID_ and the _caller ID_. 


## Running the example

You can compile the source code and run the binaries using the simulator with the provided `Makefile`. Here is the output of the running simulation:
```bash
root@ab19158b8e32:/src/sancus_projects/minimal# make 
Makefile   README.md  main.c
root@ab19158b8e32:/src/sancus_projects/minimal# make sim
sancus-cc -I"/usr/local/share/sancus-support"/include/ -Wfatal-errors -fcolor-diagnostics -Os -g -c main.c -o main.o
sancus-ld -L"/usr/local/share/sancus-support"/lib/ --ram 16K --rom 41K  -lsm-io -ldev --inline-arithmetic --standalone --verbose -o no_mac_main.elf main.o
INFO: Found new Sancus modules:
INFO:  * my_sm: 
INFO:   - Entries: hello_entry
INFO:   - No calls to other modules
INFO:   - Unprotected calls: printf2
INFO: No existing Sancus modules found
INFO: No asm Sancus modules found
INFO: Found MSP430 install directory: /usr/msp430
INFO: Found MSP430 GCC install directory: /usr/lib/gcc/msp430/4.6.3
INFO: Using output file no_mac_main.elf
sancus-crypto --fill-macs --key 9ea784508aa04321 --verbose -o main.elf no_mac_main.elf
Start simulation
sancus-sim --ram 16K --rom 41K  main.elf
Starting Verilog simulation. Press <Ctrl-C> to get to the Icarus Verilog CLI, then "finish" to exit.
******************************
* Sancus simulation started  *
* ROM: 41984B                *
* RAM: 16384B                *
******************************
sim_file: /tmp/tmp_ng8w6tf
=== Spongent parameters ===
Rate:        18
State size: 176
===========================
=== SpongeWrap parameters ===
Rate:           16
Security:       64
Blocks in key:   4
=============================
=== File I/O ===
Input:  'sim-input.bin'
Output: 'sim-output.bin'
================
FST info: dumpfile sancus_sim.fst opened for output.

------


[main.c] hi from main with ID 0, called by -1
[main.c] enabling my_sm
New SM config: 6c9c 6dfa 029c 03a4, 0
Vendor key: 9ea784508aa04321
...............................................................................................................................................................................
SM key: 765210bb3a31040d
[main.c] sm_info
SM my_sm with ID 1 enabled      : 0x6c9c 0x6dfa 0x029c 0x03a4
[main.c] calling my_sm
[main.c] hi from my_sm with ID 1, called by 0
[main.c] back in main
 ===============================================
|               SIMULATION PASSED               |
 ===============================================
=> Simulation finished.
```
