## Altera Cyclone II example

openMSP430 FPGA Projects top level directory

```
fpga/altera_de1_board                Altera FPGA Project based on Cyclone II Starter Development Board
├── bench                            Top level testbench directory
│   └── verilog                      
│       ├── altsyncram.v             Altera verilog model of the altsyncram module.       
│       ├── msp_debug.v              Testbench instruction decoder and ASCII chain generator for easy debugging
│       ├── registers.v              Connections to Core internals for easy debugging
│       ├── tb_openMSP430_fpga.v     FPGA testbench top level module
│       └── timescale.v              Global time scale definition for simulation. 
├── doc                              Diverse documentation
│   ├── DE1_Board_Schematic.pdf      Cyclone II FPGA Starter Development Board Schematics
│   ├── DE1_Reference_Manual.pdf     Cyclone II FPGA Starter Development Board Reference Manual
│   └── DE1_User Guide.pdf           Cyclone II FPGA Starter Development Board User Guide
│
├── README                           README file
├── rtl                              RTL sources
│   └── verilog                      
│       ├── driver_7segment.v        Four-Digit, Seven-Segment LED Display driver
│       ├── ext_de1_sram.v           Interface with altera DE1s external async SRAM (256kwords x 16bits)
│       ├── io_mux.v                 I/O mux for port function selection.
│       ├── openmsp430               Local copy of the openMSP430 core. 
│       │   ├── ...                  The *define.v file has been adjusted to the requirements of the project.
│       │   ├── ...
│       │   └── periph
│       │       └── ...
│       ├── OpenMSP430_fpga.v        FPGA top level file
│       ├── ram16x512.v              Single port RAM generated with the megafunction wizard
│       └── rom16x2048.v             Single port ROM generated with the megafunction wizard
│
├── sim                              Top level simulations directory
│   └── rtl_sim                      RTL simulations
│       ├── bin                      RTL simulation scripts
│       │   ├── ihex2mem.tcl         Verilog program memory file generation
│       │   ├── msp430sim            Main simulation script
│       │   ├── omsp_config.sh       
│       │   └── rtlsim.sh            Verilog Icarus simulation script
│       ├── run                      For running RTL simulations
│       │   ├── run                  Run simulation of a given software project
│       │   └── run_disassemble      Disassemble the program memory content of the latest simulation
│       └── src                      RTL simulation verilog stimulus
│           ├── *.v                  Stimulus vector for the corresponding software project
│           └── submit.f             Verilog simulator command file
│
├── software                         Software C programs to be loaded in the program memory
│   ├── bin                          Specific binaries required for software development.
│   │   ├── mifwrite                 Linux executable.
│   │   ├── mifwrite.exe             Windows executable.
│   │   └── mifwrite.cpp       This prog is taken from http://www.johnloomis.org/ece595c/notes/isa/mifwrite.html
│   │                          and slightly changed to satisfy quartus6.1 *.mif eating engine.
│   │
│   └── memledtest                   LEDs blinking application (from the CDK4MSP project)
│       ├── ...
│       └── README
│
└── synthesis                        Top level synthesis directory
    └── altera
        ├── main.qsf                 Global Assignments file
        ├── main.sof                 SOF file
        ├── OpenMSP430_fpga.qpf      Quartus II project file
        └── openMSP430_fpga_top.v    RTL file list to be synthesized
```


#### Directory structure: Software Development Tools

```
tools                                 openMSP430 Software Development Tools top level directory
├── bin                               Contains the main TCL scripts (and windows executable files if generated)
│   ├── gen-coe.py                    
│   ├── openmsp430-gdbproxy.tcl       GDB Proxy server to be used together with MSP430-GDB and the Eclipse,
│   │                                 DDD, or Insight graphical front-ends
│   ├── openmsp430-loader.tcl         Simple command line boot loader
│   ├── openmsp430-minidebug.tcl      Minimalistic debugger with simple GUI
│   └── README.TXT                    README file regarding the use of TCL scripts in a Windows environment.
│
├── lib                               Common library
│   └── tcl-lib                       Common TCL library
│       ├── combobox.tcl              A combobox listbox widget written in pure tcl (from Bryan Oakley)
│       ├── dbg_functions.tcl         Main utility functions for the openMSP430 serial debug interface
│       ├── dbg_uart.tcl              Low level UART communication functions
│       └── xml.tcl                   Simple XML parser (from Keith Vetter)
│
├── omsp_alias.xml                    This XML file allows the software development tools to identify a
│                                     openMSP430 implementation, and add customized extra
│                                     information (Alias, URL, ...).
│
└── openmsp430-gdbproxy               GDB Proxy server main project directory
    ├── commands.tcl                  RSP command execution functions.
    ├── openmsp430-gdbproxy.tcl       GDB Proxy server main TCL Script (symbolic link with script in bin)
    ├── server.tcl                    TCP/IP Server utility functions. Send/Receive RSP packets from GDB.
    └── doc                           Some documentation regarding GDB and the RSP protocol.
        ├── ew_GDB_RSP.pdf            Document from Bill Gatliff: Embedding with GNU: the gdb Remote
        │                             Serial Protocol
        └── Howto-GDB_Remote_Serial_Protocol.pdf
                                      Document from Jeremy Bennett (Embecosm): Howto: GDB Remote Serial
                                      Protocol - Writing a RSP Server
```


### relevant directories

The _Altera DE1 board_ is located in:

[sancus-core/fpga/altera_de1_board](https://github.com/sancus-pma/sancus-core/fpga/altera_de1_board)

Also interesting directory:

[sancus-core/core/synthesis/altera](https://github.com/sancus-pma/sancus-core/core/synthesis/altera)


#### Readmes in sancus-core

```bash
───────┬───────────────────────────────────────────────────────────────────────────────────────────────────────
       │ File: Repos/sancus-core/fpga/altera_de1_board/README
───────┼───────────────────────────────────────────────────────────────────────────────────────────────────────
   1   │ This is OpenMSP430 core+peripherals implementation adapted for Altera DE1 board.
   2   │ 
   3   │ It is based on original Olivier's adaptation for Diligent S3 board, but has following distinctions:
   4   │ 
   5   │ 1. Fixed 7segment core, since DE1 has non-muxed digits.
   6   │ 2. It is adapted for MegaWizard-generated 16-bit wide on-chip ROMs and RAMs.
   7   │ 3. Debug ROM write is removed (although it shouldn't be a problem to return it back).
   8   │    Anyway I haven't used any debug features.
   9   │ 4. As an alternative to the embedded synchronous RAM, there is ext_de1_sram module that
  10   │    allows core to access external on-board static RAM.
  11   │ 5. Core is configured to have non-standard ROM and RAM sizes (4kB and 1kB), so make
  12   │    sure the OpenMSP430_defines.v file is properly updated
  13   │ 6. There is new software project that uses custom linker script to compile for non-standard
  14   │    ROM and RAM sizes.
  15   │ 
  16   │ Any questions? lvd.mhm@gmail.com
  17   │ 
───────┴───────────────────────────────────────────────────────────────────────────────────────────────────────
```

```bash
───────┬────────────────────────────────────────────────────────────────────────────────────
       │ File: Repos/sancus-core/fpga/altera_de1_board/software/memledtest/README
───────┼────────────────────────────────────────────────────────────────────────────────────
   1   │ This project is for altera DE1 implementation of OpenMSP430 core and periphery.
   2   │ It is adopted from original "leds" project.
   3   │ 
   4   │ It does simple things using two periodic interrupts (from watchdog and
   5   │ from timerA), also it tests extensively RAM by creating some continuous
   6   │ traffic with it.
   7   │ 
   8   │ It is tuned for non-standard sizes of ROM (4kB or 2kWords) and
   9   │ RAM (1kB or 512 words), thus it has its own linker script (link.ld).
  10   │ Also it generates *.mif as a final result to use in Quartus, which,
  11   │ in turn, is generated from raw *.bin file instead of *.hex.
  12   │ This is done because Q6.1 can't load byte-wise ihex to the
  13   │ 16bit-wide ROM.
  14   │ 
  15   │ Warning! You should change the OpenMSP430_defines.v file to adopt for
  16   │ non-standard memory sizes.
  17   │ 
  18   │ mifwrite.exe is compiled for win32; you can find its source
  19   │ in tools/bin
  20   │ 
  21   │ Any questions? lvd.mhm@gmail.com
───────┴────────────────────────────────────────────────────────────────────────────────────
```


### important verilog files

```bash
───────┬────────────────────────────────────────────────────────────────────────────────────
       │ File: Repos/sancus-core/core/synthesis/altera/design_files.v
───────┼────────────────────────────────────────────────────────────────────────────────────
   1   │ //----------------------------------------------------------------------------
   2   │ // Copyright (C) 2001 Authors
   3   │ //
   4   │ // ...
  23   │ //----------------------------------------------------------------------------
  24   │ // 
  25   │ // *File Name: openMSP430_fpga_top.v
  26   │ // ...
  35   │ 
  36   │ //=============================================================================
  37   │ // FPGA Specific modules
  38   │ //=============================================================================
  39   │ `include "../src/arch.v"
  40   │ `include "../src/openMSP430_fpga.v"
  41   │ 
  42   │ `ifdef CYCLONE_II
  43   │    `include "../src/megawizard/cyclone2_pmem.v"
  44   │    `include "../src/megawizard/cyclone2_dmem.v"
  45   │ `endif
  46   │ `ifdef CYCLONE_III
  47   │    `include "../src/megawizard/cyclone3_pmem.v"
  48   │    `include "../src/megawizard/cyclone3_dmem.v"
  49   │ `endif
  50   │ `ifdef CYCLONE_IV_GX
  51   │ ....
───────┴────────────────────────────────────────────────────────────────────────────────────
```

```bash
───────┬────────────────────────────────────────────────────────────────────────────────────
       │ File: Repos/sancus-core/core/synthesis/altera/src/arch.v
───────┼────────────────────────────────────────────────────────────────────────────────────
   1   │ 
   2   │ `define CYCLONE_II
   3   │ 
───────┴────────────────────────────────────────────────────────────────────────────────────
```

```bash
───────┬────────────────────────────────────────────────────────────────────────────────────
       │ File: Repos/sancus-core/fpga/altera_de1_board/synthesis/altera/openMSP430_fpga_top.v
───────┼────────────────────────────────────────────────────────────────────────────────────
   1   │ //----------------------------------------------------------------------------
   2   │ // Copyright (C) 2001 Authors
   3   │ //
   4   │ // ...
  23   │ //----------------------------------------------------------------------------
  24   │ //
  25   │ // *File Name: openMSP430_fpga_top.v
  26   │ // ...
  35   │ 
  36   │ //=============================================================================
  37   │ // FPGA Specific modules
  38   │ //=============================================================================
  39   │ 
  40   │ `include "../../../rtl/verilog/openMSP430_fpga.v"
  41   │ `include "../../../rtl/verilog/io_mux.v"
  42   │ `include "../../../rtl/verilog/driver_7segment.v"
  43   │ `include "../../../rtl/verilog/ram16x512.v"    // altera DE1 specific modules
  44   │ `include "../../../rtl/verilog/rom16x2048.v"   //
  45   │ `include "../../../rtl/verilog/ext_de1_sram.v" //
  46   │ 
  47   │ 
  48   │ //=============================================================================
  49   │ // openMSP430
  50   │ //=============================================================================
  51   │ 
  52   │ `include "../../../rtl/verilog/openmsp430/openMSP430.v"
  53   │ `include "../../../rtl/verilog/openmsp430/omsp_frontend.v"
  54   │ `include "../../../rtl/verilog/openmsp430/omsp_execution_unit.v"
  55   │ `include "../../../rtl/verilog/openmsp430/omsp_register_file.v"
  56   │ `include "../../../rtl/verilog/openmsp430/omsp_alu.v"
  57   │ `include "../../../rtl/verilog/openmsp430/omsp_sfr.v"
  58   │ `include "../../../rtl/verilog/openmsp430/omsp_mem_backbone.v"
  59   │ `include "../../../rtl/verilog/openmsp430/omsp_clock_module.v"
  60   │ `include "../../../rtl/verilog/openmsp430/omsp_dbg.v"
  61   │ `include "../../../rtl/verilog/openmsp430/omsp_dbg_hwbrk.v"
  62   │ `include "../../../rtl/verilog/openmsp430/omsp_dbg_uart.v"
  63   │ `include "../../../rtl/verilog/openmsp430/omsp_watchdog.v"
  64   │ `include "../../../rtl/verilog/openmsp430/omsp_multiplier.v"
  65   │ `include "../../../rtl/verilog/openmsp430/omsp_sync_reset.v"
  66   │ `include "../../../rtl/verilog/openmsp430/omsp_sync_cell.v"
  67   │ `include "../../../rtl/verilog/openmsp430/omsp_scan_mux.v"
  68   │ `include "../../../rtl/verilog/openmsp430/omsp_and_gate.v"
  69   │ `include "../../../rtl/verilog/openmsp430/omsp_wakeup_cell.v"
  70   │ `include "../../../rtl/verilog/openmsp430/omsp_clock_gate.v"
  71   │ `include "../../../rtl/verilog/openmsp430/omsp_clock_mux.v"
  72   │ `include "../../../rtl/verilog/openmsp430/periph/omsp_gpio.v"
  73   │ `include "../../../rtl/verilog/openmsp430/periph/omsp_timerA.v"
  74   │ 
───────┴────────────────────────────────────────────────────────────────────────────────────
```


#### altera DE1 specific modules

- [fpga/rtl/verilog/driver_7segment.v](https://github.com/sancus-pma/sancus-core/fpga/rtl/verilog/driver_7segment.v) -- maybe not DE1 specific
- [fpga/rtl/verilog/ram16x512.v](https://github.com/sancus-pma/sancus-core/fpga/rtl/verilog/ram16x512.v)       -- included by fpga/altera_de1_board/synthesis/altera/openMSP430_fpga_top.v
- [fpga/rtl/verilog/rom16x2048.v](https://github.com/sancus-pma/sancus-core/fpga/rtl/verilog/rom16x2048.v)      -- included by fpga/altera_de1_board/synthesis/altera/openMSP430_fpga_top.v
- [fpga/rtl/verilog/ext_de1_sram.v](https://github.com/sancus-pma/sancus-core/fpga/rtl/verilog/ext_de1_sram.v)    -- included by fpga/altera_de1_board/synthesis/altera/openMSP430_fpga_top.v


#### cyclone II specific modules

- [core/synthetis/altera/src/megawizard/cyclone2_pmem.v](https://github.com/sancus-pma/sancus-core/core/synthetis/altera/src/megawizard/cyclone2_pmem.v)  -- by core/synthesis/altera/design_files.v
- [core/synthetis/altera/src/megawizard/cyclone2_dmem.v](https://github.com/sancus-pma/sancus-core/core/synthetis/altera/src/megawizard/cyclone2_dmem.v)  -- by core/synthesis/altera/design_files.v

dmem = data memory

pmem = program memory


#### Difference Cyclone II and Cyclone III

```bash
diff core/synthesis/altera/src/megawizard/cyclone2_dmem.v core/synthesis/altera/src/megawizard/cyclone3_dmem.v
```

```diff
7c7
< // File Name: cyclone2_dmem.v
---
> // File Name: cyclone3_dmem.v
39c39
< module cyclone2_dmem (
---
> module cyclone3_dmem (
96c96
< 		altsyncram_component.intended_device_family = "Cyclone II",
---
> 		altsyncram_component.intended_device_family = "Cyclone III",
103a104
> 		altsyncram_component.read_during_write_mode_port_a = "NEW_DATA_NO_NBE_READ",
129c130
< // Retrieval info: PRIVATE: INTENDED_DEVICE_FAMILY STRING "Cyclone II"
---
> // Retrieval info: PRIVATE: INTENDED_DEVICE_FAMILY STRING "Cyclone III"
150c151
< // Retrieval info: CONSTANT: INTENDED_DEVICE_FAMILY STRING "Cyclone II"
---
> // Retrieval info: CONSTANT: INTENDED_DEVICE_FAMILY STRING "Cyclone III"
157a159
> // Retrieval info: CONSTANT: READ_DURING_WRITE_MODE_PORT_A STRING "NEW_DATA_NO_NBE_READ"
176,183c178,185
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem.v TRUE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem.inc TRUE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem.cmp FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem.bsf FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem_inst.v TRUE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem_bb.v TRUE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem_waveforms.html TRUE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_dmem_wave*.jpg FALSE
---
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem.v TRUE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem.inc FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem.cmp FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem.bsf FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem_inst.v FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem_bb.v FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem_waveforms.html FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_dmem_wave*.jpg FALSE
```

```bash
diff core/synthesis/altera/src/megawizard/cyclone2_pmem.v core/synthesis/altera/src/megawizard/cyclone3_pmem.v
```

```diff
7c7
< // File Name: cyclone2_pmem.v
---
> // File Name: cyclone3_pmem.v
39c39
< module cyclone2_pmem (
---
> module cyclone3_pmem (
96c96
< 		altsyncram_component.intended_device_family = "Cyclone II",
---
> 		altsyncram_component.intended_device_family = "Cyclone III",
103a104
> 		altsyncram_component.read_during_write_mode_port_a = "NEW_DATA_NO_NBE_READ",
129c130
< // Retrieval info: PRIVATE: INTENDED_DEVICE_FAMILY STRING "Cyclone II"
---
> // Retrieval info: PRIVATE: INTENDED_DEVICE_FAMILY STRING "Cyclone III"
150c151
< // Retrieval info: CONSTANT: INTENDED_DEVICE_FAMILY STRING "Cyclone II"
---
> // Retrieval info: CONSTANT: INTENDED_DEVICE_FAMILY STRING "Cyclone III"
157a159
> // Retrieval info: CONSTANT: READ_DURING_WRITE_MODE_PORT_A STRING "NEW_DATA_NO_NBE_READ"
176,183c178,185
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem.v TRUE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem.inc FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem.cmp FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem.bsf FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem_inst.v FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem_bb.v FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem_waveforms.html FALSE
< // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone2_pmem_wave*.jpg FALSE
---
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem.v TRUE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem.inc FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem.cmp FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem.bsf FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem_inst.v FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem_bb.v FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem_waveforms.html FALSE
> // Retrieval info: GEN_FILE: TYPE_NORMAL cyclone3_pmem_wave*.jpg FALSE
```

