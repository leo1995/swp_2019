# SWP - Sancus: Lightweight and Open-Source Trusted Computing for the IoT

During our software project in WS19/20 we worked with Sancus, a security architecture targeting
resource-constrained embedded devices, which often times don't have dedicated memory protection
mechanisms like an MPU or an MMU. 

By utilizing newly created processor instructions and a small modification to the memory
access logic of the processor, Sancus provides isolation for software modules, remote
attestation of their states and secure communication between the modules/ those who provided
them, and all of that without trusing _any_ software on the processor.
([Read more about Sancus](docs/README.md))

During our software project, our goal was to run a Sancus-enabled system on an FPGA. Since
the board used by the original prototype is outdated and unavailable, we tried to deploy Sancus
on a recent FPGA board, the DE10-Standard by Altera.

Unfortunately, we didn't quite reach this goal. Becaus of that, our project is two-fold:
On the one hand, despite not being able to finish the project, we came quite close to get
OpenMSP430 (the processor implementation that Sancus extended) running. We hope that our
insights can be used by others to finish what we started.
On the other hand, we looked into compiling and simulating Sancus-enabled projects, wrote our
own examples and wrote detailed documentation.


## Repository overview

Here is a small overview about our repository structure, to brief which files you can find in the folders:

- _docker/_ - directory for building the toolchain using _Docker_

- _docs/_ - all (external) documentations and user manuals 

- _presentation/_ - our results for the presentation of this project 

- _Repos/_ - directory for all repositories from [Sancus](https://github.com/sancus-pma "Sancus repositories").
  
  There are also installation instructions for building the Sancus toolchain on your host system. 

- _src/_ - directory for our source code, i. e. software modules to deploy 

- _tools/_ - external tools needed for this project, i. e. the CD contents from our development board 

- _wiki/_ - not used, contains only a link list to documentation documents in the main repository)

## Installing the toolchain

For ArchLinux see [build_documentation_arch.md](Repos/build_documentation_arch.md) or for Ubuntu see [build_documentation_ubuntu.md](Repos/build_documentation_ubuntu.md).

## Links

* [Sancus Website](https://distrinet.cs.kuleuven.be/software/sancus/ "KU Leuven Sancus")
